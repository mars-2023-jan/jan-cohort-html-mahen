let a = [1,3,5,7]
let b = [2,4,6,8]

let car = {
    make: 'Ford',
    model: 'Mustang',
    year: 1999
}

let updatedCar ={
    color: 'red',
    year: 2018,

}

let c = {...car, ...updatedCar}

console.log(c)