class User {
    #name
    #age
    
    constructor(name, age){
        this.#name = name;
        this.#age = age
    }
   
   set name(name){
    this.#name = name
   }
   set age(age){
    this.#age = age
   }

   get name(){
    return this.#name
   }

   get age(){
    return this.#age
   }
}

let user1 = new User('David', 34);
console.log(user1.name)
