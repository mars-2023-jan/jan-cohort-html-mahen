let nameArr = []
let emailArr = []

let myEl = document.getElementById("my-btn")

myEl.addEventListener('click', function(){
    alert(this.innerHTML)
})

function addData(){
    let email = document.getElementById("exampleInputEmail1").value
    let name = document.getElementById("exampleInputName").value

    nameArr.push(name)
    emailArr.push(email)

    localStorage.setItem('nameData', JSON.stringify(nameArr))
    localStorage.setItem('emailData', JSON.stringify(emailArr))
}

let employees = []
let student = {}
let text = ''
function addObject(){
    student.email = document.getElementById("exampleInputEmail1").value
    student.name = document.getElementById("exampleInputName").value
    student.grade = document.getElementById("exampleInputGrade").value
    student.year = document.getElementById("exampleInputYear").value

    if(student.grade == 'A+'){
        text = student.name +' is a very good student'
    }

    document.getElementById("display").innerHTML = text
}

// create an html page that will display employee details like empId, empName, designation, hoursWorked
// and a button to submit the form

// create the employee object in javascript and populate it with the values from html form
// if designation is 'Manager' then salary = hoursworked * 50
// if desgnation is 'Consultant' then salary = hoursWorked * 30
// if designation is 'Trainee' then salary = hoursWorked * 20

// Display : Mark who is a Manager gets $20000 