
let display = ({firstName, lastName})=>console.log(firstName +' '+ lastName)

let person = {
    firstName: 'Michael',
    lastName: 'Jordan',
    age:    31

}

display(person)

let salaries = {
    "John": 500,
    "Mary": 800,
    "Carl": 600
}

function maxSalary(salaries){
    let maxSalary = 0
    let maxName = null

    for(const[name, salary] of Object.entries(salaries)){
        if(maxSalary < salary){
            maxSalary = salary
            maxName = name
        }
    }
    return maxName
}

console.log(Object.keys(salaries))

console.log(Object.values(salaries))

//console.log(maxSalary(salaries))

//let entries = Object.entries(salaries)

//console.log(entries)


//let{firstName, middleName='', lastName, age=29} = person()

//console.log(firstName+' '+middleName+' '+lastName+':'+age)

