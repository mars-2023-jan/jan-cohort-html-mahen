// let employee = {          //Object literal
//     'firstName' : 'John',
//     'lastName' : 'Doe',
//     'age' : 32,
//     'designation' : 'Manager',
//     'home address' : 'Milwaukee',
//      getFullName(){
//         return this.firstName+' '+this.lastName
//     }

// }

// let permanentEmp = {
//     'hireDate' : '03-04-2010'
// }


// permanentEmp.__proto__ = employee

// console.log(permanentEmp)
//console.log(employee)

function Car(make, model, year, color){
    this.make = make
    this.model=model
    this.year=year
    this.color=color
}

let car1 = new Car('Honda', 'Accord', 2016, 'Blue')
let car2 = new Car('Ford', 'Freestyle', 2016, 'Red')

Car.prototype.getDetail = function(){
    return this.make + ' '+this.model
}

console.log(car1.getDetail())
console.log(car2.getDetail())


// 
// let car3 = new Car('Toyota', 'Forrunner', 2016, 'White')

// console.log(car1.model)

// //console.log(employee.home address)

// employee.firstName = 'Pete'

// employee.contact = 247125892

// delete employee['home address']

// console.log(employee)

// console.log('age' in employee)

// console.log('ssn' in employee)
// console.log('===================')
// for(const key in employee){
//     console.log(employee[key])
// }
// console.log('===================')
// console.log(employee.getFullName())

// //console.log(employee['home address'])

// function Employee(empId, empName, empDesignation, hoursWorked, salary){
//     this.empId = empId
//     this.empName = empName
//     this.empDesignation = empDesignation
//     this.hoursWorked = hoursWorked
//     this.salary = salary
// }

