let myFunc = function(){   //function expression
    console.log('Example of a function expression')
}

setTimeout(()=> console.log('Display after 5 seconds'),5000)

myFunc();

(function(){              //Immediately called anonymous function
    console.log('Anonymous function')
})();

let sum = function(a,b){
    return a+b
}

let arrow = () => console.log('Arrow function')


console.log(sum(10,20))

arrow();